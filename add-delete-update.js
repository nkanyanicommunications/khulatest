﻿var pgp = require('pg-promise')(/*options*/);
const config = require('./config.json');

var connection = {
  host: config.host,
  user: config.user,
  password: config.password,
  port: config.port,
  database: config.database
};

module.exports = {
  saveToDatabase: saveToDatabase,
  //updateToDatabase: updateToDatabase,
  //deleteFromDatabase: deleteFromDatabase

};

function validateEntries(data) {
  data.map((value, index) => {
    if (value.name === undefined || value.name === "") {
      throw new Error("please enter your name.");
    }
    if (value.surname === undefined || value.surname === "") {
      throw new Error("please enter your surname.");
    }
    if (value.idnumber === undefined || value.idnumber === "") {
      throw new Error("please enter your id number.");
    }
  })
}

function saveToDatabase(data, connection) {
  try {
    validateEntries(data);
    const action = 'INSERT INTO nkanyani.khula(name, surname, idnumber, datecreated) VALUES(${name}, ${surname}, ${idnumber}, ${date})';
    return dataBaseInteractions(action, data, connection).then(interactionResponse => {
      return interactionResponse;
    }).catch(error => {
      console.log(error);
      throw error;
    });
  } catch (error) {
    console.log(error);
    return Promise.reject(error);
  }
};

function updateToDatabase(data, connection) {

  try {
    validateEntries(data);
    const action = 'UPDATE nkanyani.khula SET name = ${name}, surname = ${surname}, idnumber = ${idnumber}, datecreated = ${date} WHERE id =  ${id}';
    return dataBaseInteractions(action, data, connection).then(interactionResponse => {
      return interactionResponse;
    }).catch(error => {
      console.log(error);
      throw error;
    });
  } catch (error) {
    console.log(error);
    return Promise.reject(error);
  }
};

function deleteFromDatabase(data, connection) {
  try {
  const action = 'DELETE FROM nkanyani.khula WHERE id =  ${id}';
  return dataBaseInteractions(action, data, connection).then(interactionResponse => {
    return interactionResponse;
  }).catch(error => {
    console.log(error);
    throw error;
  });
} catch (error) {
  console.log(error);
  return Promise.reject(error);
}
};

// generate function for adding, updating and deleting based on the action provided
function dataBaseInteractions(action, data, connection) {
  return createConnectToDatabase(connection).then(dataBaseConnection => {
    return dataBaseConnection.tx(dataResponse => {
      var queries = data.map(u => {
        u.date = new Date().toISOString();
        return dataResponse.none(action, u);
      });
      return queries;
    }).then(data => {
      // OK
      console.log("Data added/deleted/updated successfully.");
      return true;
    }).catch(error => {
      // Error
      throw error;
    });
  });
};

function createConnectToDatabase(event) {
  return new Promise((resolve, reject) => {
    try {
      const connection = pgp(event);
      resolve(connection);
    } catch (error) {
      reject(error)
    };
  });
};